<?php
namespace Framework\Controller;

use Framework\DInjector\Singleton;
use Framework\Reporter\DevReporter;
use Framework\Database\Stacky\Model;
use Framework\Template\TemplateInterface;

/**
 * @class BaseController
 * Contains all the basic Controller methods
 */
abstract class AbstractController
{

    /**
     * Load Model Class in Controller
     * @param string name
     * @return Framework\Database\Stacky\Model
     * @deprecated since version 4
     */
    protected function loadModel(string $modelClass): Model
    {
        return new $modelClass();
    }

    /**
     * Load View Class in Controller
     * @param string $viewFile
     * @return \Framework\Template\TemplateInterface
     * @deprecated since version 4
     */
    protected function loadView(string $viewFile, $template = TEMPLATE_SITE, string $controller = ''): TemplateInterface
    {
        if ($controller === '') {
            $controllerName = static::class;
            $controller = str_replace('Controller', null, ltrim(substr($controllerName, strrpos($controllerName, NS)), NS));
        }

        $templateHandler = NS . 'App' . NS . 'Templates' . NS . $template . NS . 'TemplateHandler';

        $template_object = new $templateHandler(APP_DIR . DS . 'Views' . DS . $controller . DS . $viewFile . '.php');

        if (isDebug()) {
            $devReporter = Singleton::get(DevReporter::class);

            $devReporter->setCurrentController($controller);
            $devReporter->setCurrentMethod(debug_backtrace()[1]['function']);
            $devReporter->setCurrentView($viewFile);
            $devReporter->pushToMemoryStack($controller);
        }

        return $template_object;
    }
}
